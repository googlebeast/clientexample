#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#define RESP_SIZE 100

// Source: https://stackoverflow.com/questions/8465006/how-do-i-concatenate-two-strings-in-c
char* concat(int count, ...)
{
    va_list ap;
    int i;

    // Find required length to store merged string
    int len = 1; // room for NULL
    va_start(ap, count);
    for(i=0 ; i<count ; i++)
        len += strlen(va_arg(ap, char*));
    va_end(ap);

    // Allocate memory to concat strings
    char *merged = calloc(sizeof(char),len);
    int null_pos = 0;

    // Actually concatenate strings
    va_start(ap, count);
    for(i=0 ; i<count ; i++)
    {
        char *s = va_arg(ap, char*);
        strcpy(merged+null_pos, s);
        null_pos += strlen(s);
    }
    va_end(ap);

    return merged;
}

char* srtrev(char *str)
{
	int i;
    int size = strlen(str);
    char *res = malloc((size + 1) * sizeof(char));
    for(i=0; i<size; i++) {
        res[i] = str[size-i-1];
    }
	res[size] = '\0';
    return res;
}

int calc(void) {

	int client_socket;
	int return_value;
	char server_response[RESP_SIZE];
	struct sockaddr_in my_addr;
	char op[100];
	char o1[100];
	char o2[100];

	/* create socket */
	client_socket = socket(AF_INET, SOCK_STREAM, 0);
	memset(&my_addr, 0, sizeof(struct sockaddr_in));
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(2000);
	my_addr.sin_addr.s_addr = inet_addr("35.222.223.14");

	/* connect socket on given adress */
	return_value = connect(client_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr_in));
	if (return_value == 0) {
		printf("Connect - OK\n");
	} else {
		printf("Connect - ERR\n");
		return 1;
	}

	return_value = recv(client_socket, server_response, RESP_SIZE, 0);
	if (return_value >= 0) {
		printf("%s\n", server_response);
	} else {
		printf("Recieve - ERR\n");
		return 2;
	}
	
	memset(&server_response, 0, RESP_SIZE);
	return_value = recv(client_socket, server_response, RESP_SIZE, 0);
	if (return_value >= 0) {
		printf("%s\n", server_response);
	} else {
		printf("Recieve - ERR\n");
		return 2;
	}

	printf("\nEnter operation: ");
	fgets(op,100,stdin);
	
	printf("\nEnter operand 1: ");
	fgets(o1,100,stdin);
	
	printf("\nEnter operand 2: ");
	fgets(o2,100,stdin);

	char* req = concat(6, op, "|", o1, "|", o2, "\n");

	return_value = send(client_socket, req, strlen(req), 0);
	if(return_value < 0) {
		printf("Error in sending");
		free(req);
		return 3;
	}
	
	free(req);

	memset(&server_response, 0, RESP_SIZE);
	return_value = recv(client_socket, server_response, RESP_SIZE, 0);
	if(return_value < 0)
	{
		printf("Recieve - ERR\n");
		return 2;
	}
	printf("Result: %s\n", server_response);
	
	close(client_socket);
	return 0;
}

int echo(void) {

	int client_socket;
	int return_value;
	char server_response[RESP_SIZE];
	struct sockaddr_in my_addr;
	char msg[100];

	/* create socket */
	client_socket = socket(AF_INET, SOCK_STREAM, 0);
	memset(&my_addr, 0, sizeof(struct sockaddr_in));
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(2001);
	my_addr.sin_addr.s_addr = inet_addr("35.222.223.14");

	/* connect socket on given adress */
	return_value = connect(client_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr_in));
	if (return_value == 0) {
		printf("Connect - OK\n");
	} else {
		printf("Connect - ERR\n");
		return 1;
	}

	printf("\nEnter message: ");
	fgets(msg,100,stdin);

	return_value = send(client_socket, msg, strlen(msg), 0);
	if(return_value < 0) {
		printf("Error in sending");
		return 3;
	}

	memset(&server_response, 0, RESP_SIZE);
	return_value = recv(client_socket, server_response, RESP_SIZE, 0);
	if(return_value < 0)
	{
		printf("Recieve - ERR\n");
		return 2;
	}
	printf("Response: %s\n", server_response);
	
	close(client_socket);
	return 0;
}

int reverse(void) {

	int client_socket;
	int return_value;
	char server_response[RESP_SIZE];
	struct sockaddr_in my_addr;

	/* create socket */
	client_socket = socket(AF_INET, SOCK_STREAM, 0);
	memset(&my_addr, 0, sizeof(struct sockaddr_in));
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(2002);
	my_addr.sin_addr.s_addr = inet_addr("35.222.223.14");

	/* connect socket on given adress */
	return_value = connect(client_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr_in));
	if (return_value == 0) {
		printf("Connect - OK\n");
	} else {
		printf("Connect - ERR\n");
		return 1;
	}

	memset(&server_response, 0, RESP_SIZE);
	return_value = recv(client_socket, server_response, RESP_SIZE, 0);
	if(return_value < 0)
	{
		printf("Recieve - ERR\n");
		return 2;
	}

	char* tmp = strrev(server_response);

	return_value = send(client_socket, concat(2, tmp , "\n"), strlen(server_response), 0);
	if(return_value < 0) {
		printf("Error in sending");
		free(tmp);
		return 3;
	}
	free(tmp);

	memset(&server_response, 0, RESP_SIZE);
	return_value = recv(client_socket, server_response, RESP_SIZE, 0);
	if(return_value < 0)
	{
		printf("Recieve - ERR\n");
		return 2;
	}
	printf("Response: %s\n", server_response);
	
	close(client_socket);
	return 0;
}

int main( int argc, char *argv[] )  {

   if( argc == 2 ) {
      if(!strcmp(argv[1],"0")) {
		return calc();
	  } else if(!strcmp(argv[1], "1")) {
		return echo();
	  } else if(!strcmp(argv[1], "2")) {
		return reverse();
	  } else {
		  printf("Invalid argument - expected 0 / 1 / 2 for ports 2000 / 2001 / 2002");
		  return 4; 
	  }
	  
   } else {
      printf("One argument expected.\n");
   }
}
